#! /usr/bin/env python
from generators import *
from pathlib import Path
import os, errno
import click
from pprint import pprint


@click.command()
@click.argument("in_dir", type=click.Path(exists=True))
@click.option("-z", "--zephyr", is_flag=True, default=False)
@click.option("-m", "--micropython", is_flag=True, default=False)
@click.option("--force", is_flag=True, default=False)
def gen_header(in_dir, force, zephyr, micropython):
    """
    O.K.
    """
    path = Path(in_dir)
    registers = path / "registers.json"
    if not registers.exists():
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), str(registers))

    plist = path.parts
    driver_name = plist[0]

    if zephyr:
        Path(path / "zephyr").mkdir(exist_ok=True)
        out_dir = path / "zephyr"
        generator = ZephyrGenerator(registers, out_dir, driver_name, force)
        generator.gen_file()

    if micropython:
        Path(path / "mpy").mkdir(exist_ok=True)
        out_dir = path / "mpy"
        generator = MpyGenerator(registers, out_dir, driver_name, force)
        generator.gen_file()


if __name__ == "__main__":
    gen_header()
