import os, errno, json


class Generator:
    newline = "\n"

    def __init__(self, infile, outfile, force):

        with open(infile, "r") as f:
            raw = json.load(f)
            self.regmap = raw["registers"]

        self.outfile = outfile
        self.force = force

    def gen_header(self, file):
        pass

    def gen_body(self, file):
        pass

    def gen_footer(self, file):
        pass

    def gen_file(self):
        if not self.force and self.outfile.exists():
            raise FileExistsError(
                errno.EEXIST, os.strerror(errno.EEXIST), str(self.outfile)
            )
        with open(self.outfile, "w") as f:
            self.gen_header(f)
            self.gen_body(f)
            self.gen_footer(f)


class ZephyrGenerator(Generator):
    indent = "\t"

    def __init__(self, infile, out_dir, driver_name, force):
        self.name = driver_name.upper()
        outfile = out_dir / f"{driver_name.lower()}.h"
        super(ZephyrGenerator, self).__init__(infile, outfile, force)

    def gen_header(self, file):
        file.write("#ifndef ZEPHYR_DRIVERS_{name}_REGISTERS_H_\n".format(name=self.name))
        file.write("#define ZEPHYR_DRIVERS_{name}_REGISTERS_H_\n".format(name=self.name))
        file.write("\n")

    def gen_body(self, file):
        for reg in self.regmap:
            file.write(
                "#define {name}_{regname}_ADDR {regaddr}\n".format(
                    name=self.name, regname=reg["name"], regaddr=reg["addr"]
                )
            )
            if "fields" in reg:
                file.write(
                    "union {name}_{regname} {{\n".format(
                        name=self.name, regname=reg["name"]
                    )
                )
                file.write("{indent}uint8_t reg;\n".format(indent=self.indent))
                file.write("{indent}struct {{\n".format(indent=self.indent))
                for d in reg["fields"]:
                    for name, length in d.items():
                        file.write(
                            "{indent}uint8_t {name}: {length};\n".format(
                                indent=2 * self.indent, name=name, length=length
                            )
                        )
                file.write("{indent}}} __packed;\n".format(indent=self.indent))
                file.write("};\n")
                file.write("\n")

    def gen_footer(self, file):
        file.write("#endif // ZEPHYR_DRIVERS_{name}_REGISTERS_H_".format(name=self.name))


class MpyGenerator(Generator):
    indent = "    "

    def __init__(self, infile, out_dir, driver_name, force):
        self.name = driver_name.upper()
        outfile = out_dir / f"{driver_name.lower()}.py"
        super(MpyGenerator, self).__init__(infile, outfile, force)

    def gen_header(self, file):
        file.write("from machine import Pin\n\n")
        file.write("class {name}:\n".format(name=self.name))

    def gen_body(self, file):
        for reg in self.regmap:
            if not "fields" in reg:
                file.write(
                    "{indent}{regname} = {regaddr}\n".format(
                        indent=self.indent, regname=reg["name"], regaddr=reg["addr"]
                    )
                )
            else:
                file.write(
                    "{indent}{regname} = {{\n".format(
                        indent=self.indent, regname=reg["name"]
                    )
                )
                file.write(
                    '{indent}"addr": {regaddr},\n'.format(
                        indent=2 * self.indent, regaddr=reg["addr"]
                    )
                )
                file.write(
                    '{indent}"fields": {{\n'.format(
                        indent=2 * self.indent,
                    )
                )
                os = 0
                for d in reg["fields"]:
                    for name, length in d.items():
                        file.write(
                            '{indent}"{name}": {{\n'.format(
                                indent=3 * self.indent, name=name
                            )
                        )
                        file.write(
                            '{indent}"len": {length},\n'.format(
                                indent=4 * self.indent, length=length
                            )
                        )
                        file.write(
                            '{indent}"os": {os},\n'.format(
                                indent=4 * self.indent, os=os
                            )
                        )
                        file.write(
                            "{indent}}},\n".format(
                                indent=3 * self.indent,
                            )
                        )
                        os += length

                file.write(
                    "{indent}}},\n".format(
                        indent=2 * self.indent,
                    )
                )
                file.write(
                    "{indent}}}\n".format(
                        indent=self.indent,
                    )
                )

    def gen_footer(self, file):
        pass
